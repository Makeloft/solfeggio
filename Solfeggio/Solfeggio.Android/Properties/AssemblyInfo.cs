﻿using System.Reflection;

using Android.App;

using static Android.Manifest.Permission;

[assembly: UsesPermission(Internet)]
[assembly: UsesPermission(AccessNetworkState)]
[assembly: UsesPermission(WriteExternalStorage)]
[assembly: UsesPermission(RecordAudio)]

[assembly: AssemblyTitle("S O L F E G G I O")]
[assembly: AssemblyProduct("Solfeggio")]
[assembly: AssemblyCopyright("© Makeloft Studio")]
[assembly: AssemblyVersion("5.2.0.0")]
